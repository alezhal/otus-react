import logo from './logo.svg';
import './App.css';
import Department from './components/Departments/Department';
import React from 'react';

class App extends React.Component {
  constructor(props) {
      super(props);
      this.state = { name: ''};
  }

  render() {return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Department />
      </header>
    </div>
  );
  }
}

export default App;
