import React from 'react'
import api from '../../api/Api'

export default class DeparmentDelete extends React.Component {
    constructor(props) {
        super(props);
        this.state = {id:''}
    }

    handleChange = event => {
        this.setState({id: event.target.value});
    }

    handleSubmit = async event => {
        event.preventDefault();
        let id = this.state.id;
        const response = await api.delete('api/2.0/Departments/'+ encodeURIComponent(id));
        this.setState({id:''});
        this.props.setStateOfParent({didUpdate: true});
    }

    render(){
        return (
            <div className='form-container'>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        <span className='form-label-text'>Department ID:</span>
                        <input type="text" name="id" value={this.state.id} onChange={this.handleChange} placeholder="ID of the department" />
                    </label>
                    <button type="submit">Delete department</button>
                </form>
            </div>
        )
    }
}