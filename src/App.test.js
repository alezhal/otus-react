import { render, screen } from '@testing-library/react';
import DepartmentPost from './components/Departments/DepartmentPost';

test('renders learn react link', () => {
  render(<DepartmentPost />);
  const button = screen.getByText(/Add department/i);
  expect(button).toBeInTheDocument();
});
